This repository contains the code developed for the publication titled "Algorithmic and System Approaches for a Stable LiFi-RF HetNet Under Transient Channel Conditions" accepted at PIMRC 2021.

Contents:

- `run_simulation_plane.py` and `run_simulation_overlap_topo.py`: to run the above alogorithm for different topologies and scenarios 

- `channel.py`: abstract channel class 
- `channel_model_lifi.py` and `channel_model_wifi.py`: LiFi and WiFi channel models

- `setup.py`: This class contains functions that perform simulation setup such as creating a topology and placing users/access points 
- `simulation.py`: Necessary classes like Setup, Channels, Controller and Resource allocation are instantiated here, and their respective functions are called. 
- `resource_allocation_algos.py`: this class contains functions with diferent resource allocation algorithms
- `controller.py`: Controller class 
- `USER.py`: User class with user attributes such user user location and rate
- `AP.py`: Access point class with its attributes such as location and users being surved 


- `process_res_simple.py`: to plot number of vertical/horizontal handovers and sum throughput
- `process_results_full.py`: to plot number of vertical handovers and sum throughput vs. different parameters (number of users, receiver orientation, speed, etc) over time

- `captures_process.py`: to process pcap captures and get mean and std VHO overhead; also plots the mean and standard deviation of the VHO in captures/overhead.png



To run the simulation, execute:

1) `run_simulation_plane.py` or `run_simulation_office_topo.py`
2) `process_res_simple.py` or `process_results_full.py`

Simulation results are saved as json files in the folder sim_results. 

Simulation parameters are the following:

- scenario: plane or indoor topology with overlaps 
- resource allocation algorithm: SMART (game theory based algorithm that considers QoS), GT_WO_DEMAND (same as SMART but without demand consideration), SNR, SINR, RATE 
- demand: user required rates (e.g. 25155 -> 1/3 of users have a demand of 25 Mbps, 1/3 - 15 Mbps, 1/3 - 5 Mbps. 
- blockage model: 0, 1, 2 as explained in the paper 
- blocking probabilities as explained in the paper 
- number of users num_users: total number of user in the simulation (for plane must be a multiple of 6) 
- mean_elevation: mean elevation angle of UE device 
- varying_rx_orientation: if set to 1, the receiver orientation is varied +- 10 deg. from mean_elevation 
- mobility_flag: if set to 1, then mobile users (only for the indoor overlap topology)
- speed: user speed (only for the indoor overlap topology); if set to -1, then varying speed. 
- vho_cost and hho_cost: vertical handover and horizontal handover efficiency

