from abc import ABC, abstractmethod


class Channel(ABC):
    def __init__(self):
        self.B_ofdm = 16.25 * 10 ** 6 / 2
        self.Q = 48
        self.spacing = 312.5 * 10 ** 3

    @abstractmethod
    def get_sinr_db(self):
        pass

    @abstractmethod
    def get_adaptive_rate(self):
        pass

