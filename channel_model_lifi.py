import numpy as np
import collections
from channel import Channel


class AbstractLiFiChannel(Channel):
    def __init__(self):
        super().__init__()
        self.theta_f = 30 * np.pi / 180
        self.rho = 0.9
        self.delta_T = 4.9 * 10 ** -9
        self.A_p = 1 / 10 ** 4
        self.chi = 1.5
        self.half_intensity_rad_angle = 30 * np.pi / 180
        self.T_s = 1.0
        self.m = -1 / np.log2(np.cos(self.half_intensity_rad_angle))
        self.k = 0.53
        self.i = 3
        self.N_l = 10 ** -21
        self.P_opt_lin = 1
        self.fc = 30 * 10 ** 6
        self.f0 = 30 * 10 ** 6
        self.snr_cod_dict_lifi = collections.OrderedDict({0: 0, 1: 0.8770, 3: 1.1758, 5: 1.4766, 8: 1.9141, 9: 2.4063,
                                                          11: 2.7305, 12: 3.3223, 14: 3.9023, 16: 4.5234,
                                                          18: 5.1152, 20: 5.5547})
        self.subcarrier_list_lifi = [(self.B_ofdm + i * self.spacing) for i in range(-26, 27)
                                     if i not in [-21, -7, 0, 7, 21]]
        self.lifi_snr_rate_mbps = {0: 0}
        self.f_sub = 10 * 10 ** 6 - 312.5 * 10 ** 3


class LiFiChannelModel(AbstractLiFiChannel):
    def __init__(self, flag_los_only, scenario):
        super().__init__()
        self.theta = None
        self.flag_los_only = flag_los_only
        if scenario == 'plane':
            self.h = 1
            self.width = 3.54
            self.length = 20
        else:
            self.h = 3
            self.width = 7
            self.length = 7
        self.A_r = self.width * self.length*2 + self.width*self.h*2 + self.length*self.h*2
        # self.varying_rx_orientation_flag = rx_orientation

    def get_los_gain(self, z, shift, positions_dict):
        phi = np.arctan(z/self.h)
        if positions_dict:
            numerator = (positions_dict['ap_y'] - positions_dict['user_y']) * np.sin(shift) + self.h * np.cos(shift)
            denominator = np.sqrt((positions_dict['ap_x'] - positions_dict['user_x'])**2 +
                                  (positions_dict['ap_y'] - positions_dict['user_y'])**2 + self.h**2)
            value = numerator/denominator
        else:
            value = 0
        self.theta = np.arccos(value)
        # print(f"Theta {self.theta*180/np.pi}")
        # print(f"Phi {phi*180/np.pi}")
        if phi <= 2 * np.pi and self.theta <= 2 * np.pi:
            if (self.theta <= self.theta_f and self.theta >= 0) or (self.theta < 0 and self.theta > -self.theta_f):
                # assert 0, "I am here"
                H_l_numerator = (self.m + 1) * self.A_p * self.chi**2 * self.T_s * (self.h/denominator)**self.m * np.cos(self.theta)
                H_l_denominator = 2*np.pi * (z**2 + self.h**2) * (np.sin(self.theta_f))**2
                H_l_lin = H_l_numerator / H_l_denominator
            else:
                H_l_lin = 0
                #print('!!!Zero chain gain because of Rx orientation!!! {}'.format(shift*180/np.pi))
            return H_l_lin
        else:
            assert 0, "Check whether pfi and theta are in radians"

    def get_H_d(self):
        """
        Diffuse component for one sub-carrier f_sub.
        """
        if self.flag_los_only:
            return 0
        else:
            fov_multiplier = np.sin(self.theta_f/2)
            numerator = self.rho * self.A_p * np.exp(-1j * 2 * np.pi * self.f_sub * self.delta_T) * fov_multiplier
            denominator = self.A_r * (1 - self.rho) * (1 + 1j * self.f_sub / self.fc)
            H_d_lin = abs(numerator / denominator)
            return H_d_lin

    def get_H_f(self):
        """
        Front-end device frequency response for one sub-carrier f_sub.
        """
        H_f_lin = np.exp(-self.f_sub / 1.44 / self.f0)
        return H_f_lin

    def get_sinr_lin(self, H_l, H_d, H_f, interference):
        H = (H_l + H_d) * H_f
        numerator = (self.k * self.P_opt_lin * H) ** 2
        noise = self.i ** 2 * self.N_l * self.B_ofdm
        snr_lin = numerator / (noise + interference)
        # print(f"Signal {numerator}, interference {interference}, snr_lin {snr_lin}")
        return snr_lin

    def get_interference_gain(self, interferring_aps_dist, positions_list, shift):
        gain_diffuse = self.get_H_d()
        gain_frontend = self.get_H_f()
        interference = 0
        for ap_dist, pos_dict in zip(interferring_aps_dist, positions_list):
            gain_los = self.get_los_gain(ap_dist, shift, pos_dict)
            interference += \
                (self.k * self.P_opt_lin * (gain_los + gain_diffuse) * gain_frontend) ** 2
        return interference

    def get_sinr_db(self, z, interferring_aps_dist, shift, positions_list_interf, positions_dict):
        gain_los = self.get_los_gain(z, shift, positions_dict)
        # print(f"Gain LOS {gain_los}")
        gain_diffuse = self.get_H_d()
        gain_frontend = self.get_H_f()
        interference = self.get_interference_gain(interferring_aps_dist, positions_list_interf, shift)
        snr_lin = self.get_sinr_lin(gain_los, gain_diffuse, gain_frontend, interference)
        snr_db = self.covert_snr_to_db(snr_lin)
        return snr_db

    def get_spectrum_efficiency(self, snr_db):
        if snr_db > 150:
            assert 0, "SNR should be in dB"
        efficiency = 0
        for key in sorted(self.snr_cod_dict_lifi.keys()):
            if key <= snr_db:
                efficiency = self.snr_cod_dict_lifi[key]
            if key > snr_db:
                break
        return efficiency

    def get_adaptive_rate(self, z, interferring_aps_dist, shift, positions_list_interf, positions_dict_ap, snr_db=None):
        subcarrier_adaptive_rate_dict = {}
        num_subcar_used = 1
        for f_sub in self.subcarrier_list_lifi:
            if not snr_db:
                snr_db = self.get_sinr_db(z, interferring_aps_dist, shift, positions_list_interf, positions_dict_ap,)
            efficiency = self.get_spectrum_efficiency(snr_db)
            rate = 2 * self.B_ofdm / self.Q * efficiency
            subcarrier_adaptive_rate_dict[f_sub] = rate
            num_subcar_used += 1
            if num_subcar_used > self.Q / 2 - 1:
                # print("Number of effective subcarriers is {}".format(num_subcar_used))
                break
        link_datarate_lifi = sum(subcarrier_adaptive_rate_dict.values())
        return link_datarate_lifi, subcarrier_adaptive_rate_dict

    def snr_and_datarate_based_on_snr(self):
        for snr_db in np.arange(1, 50, 1):
            rate = 0
            num_subcar_used = 1
            for _ in self.subcarrier_list_lifi:
                efficiency = self.get_spectrum_efficiency(snr_db)
                rate += 2 * self.B_ofdm / self.Q * efficiency
                num_subcar_used += 1
                if num_subcar_used > self.Q / 2 - 1:
                    break
            self.lifi_snr_rate_mbps[snr_db] = int(rate / 10 ** 6)

        # print("LiFi SINR --- Rate Mbps")
        # print(self.lifi_snr_rate_mbps)


    @staticmethod
    def covert_snr_to_db(snr_lin):
        if snr_lin == 0:
            return snr_lin
        snr_db = 10 * np.log10(snr_lin)
        return snr_db

    def find_max_rad(self):
        d = 0.5
        while True:
            snr_db = self.get_sinr_db(d, f_sub=self.f_sub, interferring_aps_dist=[], shift=0)
            if snr_db <= 0:
                break
            d += 0.1
        return round(d, 1)


def main():
    pass


if __name__ == "__main__":
    main()



