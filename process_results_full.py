import json
import os
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from collections import defaultdict
import scipy.stats


def change_dir(scenario):
    if scenario == 'plane':
        os.chdir('sim_results/plane')
    elif scenario == 'overlap':
        os.chdir('sim_results/overlap')


def get_all_files():
    directory = os.listdir()
    files = []
    for file in directory:
        fname, ext = os.path.splitext(file)
        if ext != '.json':
            continue
        files.append(file)
    return files


def plot_lables(self, fig_num, finame, name):
    plt.figure(fig_num)
    plt.grid()
    plt.xticks(labels=self.labels, ticks=list(range(-1, len(self.labels) - 1)))
    plt.tick_params(labelsize=10, pad=0.1)
    blue = mpatches.Patch(color='blue', label='without handover')
    green = mpatches.Patch(color='green', label='with handover')
    plt.legend(handles=[blue, green])
    plt.gcf().set_size_inches(4, 4)
    plt.savefig("{}_{}_b.png".format(finame, name), dpi=500, bbox_inches='tight', pad_inches=0.1)


def main(scenario, run_over, params):
    confidence = 0.95
    print(f"Scenario {scenario}")
    change_dir(scenario=scenario)
    files = get_all_files()
    print(f"Number of json files is {len(files)}")
    print(f"Run over {run_over}")
    print(run_over == 'num_users')
    num_vhos = defaultdict(list)
    sum_tp = defaultdict(list)
    tick_labels = []
    if run_over == 'elevations':
        x_axis_title = 'Mean Receiver Orientation (deg.)'
        for i, file in enumerate(files):
            with open(file) as f:
                data = json.load(f)
            for target in params:
                if data['mean_elevation'] == target:
                    num_vhos[target].append((data['total_num_vertical']))
                    sum_tp[target].append(np.mean(data['sum_tp']))
    if run_over == 'speeds':
        x_axis_title = 'Speed (m/s)'
        for i, file in enumerate(files):
            with open(file) as f:
                data = json.load(f)
            for target in params:
                if data['speed'] == target:
                    num_vhos[target].append((data['total_num_vertical']))
                    sum_tp[target].append(np.mean(data['sum_tp']))
    if run_over == 'num_users':
        x_axis_title = 'Number of Users'
        for i, file in enumerate(files):
            with open(file) as f:
                data = json.load(f)
            for target in params:
                if data['num_users'] == target:
                    num_vhos[target].append((data['total_num_vertical']))
                    sum_tp[target].append(np.mean(data['sum_tp']))
    if run_over == 'block_models':
        x_axis_title = 'Blockage Model'
        for i, file in enumerate(files):
            with open(file) as f:
                data = json.load(f)
            for target in params:
                if data['block_model'] == target:
                    num_vhos[target].append((data['total_num_vertical']))
                    sum_tp[target].append(np.mean(data['sum_tp']))

    num_vhos_mean = []
    sum_tp_mean = []
    num_vhos_std = []
    sum_tp_std = []
    for target in params:
        num_vhos_mean.append(np.mean(num_vhos[target]))
        sum_tp_mean.append(np.mean(sum_tp[target]))
        num_vhos_std.append(np.std(num_vhos[target]))
        # sum_tp_std.append(scipy.stats.sem(sum_tp[target]) * scipy.stats.t.ppf((1 + confidence) / 2., len(sum_tp[target]) - 1))
        # sum_tp_std.append(np.std(sum_tp[target]))
        sum_tp_std.append(
            scipy.stats.sem(sum_tp[target]) * scipy.stats.t.ppf((1 + confidence) / 2., len(sum_tp[target]) - 1))
    ##########Start Plot##################
    folder = 'plots'
    try:
        os.mkdir(folder)
    except:
        pass
    os.chdir(folder)
    #########Plotting handovers###########
    plt.figure(0)
    ind = np.arange(len(num_vhos_mean))
    # plt.errorbar(ind, num_vhos_mean, num_vhos_std, fmt='o', capsize=3)
    # plt.ylabel('Number of Vertical Handovers')
    # plt.xlabel(x_axis_title)
    plt.errorbar(ind, num_vhos_mean, num_vhos_std, fmt='o', capsize=4, markersize=4, elinewidth=2, markeredgewidth=2)
    plt.ylabel('Number of Vertical Handovers', fontsize=20)
    plt.xlabel(x_axis_title, fontsize=20)
    plt.xticks(ind, params)
    plt.grid()
    plt.savefig(f"vho_run.pdf", dpi=1200, bbox_inches='tight', pad_inches=0.1)
    plt.show()

    #########Plotting Sum throughput###########
    plt.figure(1)
    ind=np.arange(len(sum_tp_mean))
    # plt.errorbar(ind, sum_tp_mean, sum_tp_std, fmt='o', capsize=3)
    # plt.ylabel('Sum throughput (Mbps)')
    # plt.xlabel(x_axis_title)
    plt.errorbar(ind, sum_tp_mean, sum_tp_std, fmt='o', capsize=4, markersize=4, elinewidth=2, markeredgewidth=2)
    plt.ylabel('Sum throughput (Mbps)', fontsize=20)
    plt.xlabel(x_axis_title, fontsize=20)
    plt.xticks(ind, params)
    plt.grid()
    plt.savefig(f"sum_tp_run.pdf", dpi=1200, bbox_inches='tight', pad_inches=0.1)
    plt.show()


if __name__ == "__main__":
    scenario = 'plane'
    run_over = 'elevations' #'elevations', 'speeds', 'num_users', 'block_models'
    params = [0, -41, 'random'] #[0, -41, 'random'], [0, 1, 1.5], [24, 18, 12], [0, 1, 2]
    main(scenario, run_over, params)
