class ResourceAllocationAlgorithm:
    def __init__(self, algorithm):
        self.scheduler = 'PF'  # MF PF
        self.algorithm = algorithm

    def snr_or_snir_based_alg(self, controller):
        controller.scheduler = 'PF'
        controller.get_distances_from_users_to_aps()
        controller.get_sinr_from_users_to_aps()

        # debug
        # self.debug_print_user_ap_snr_and_dist(controller)

        for user in controller.user_list:
            for ap in user.aps_their_sinrs:
                if user.my_sinr < user.aps_their_sinrs[ap]:
                    user.my_sinr = user.aps_their_sinrs[ap]
                    user.my_ap = ap
                    ap.connected_users.append(user)
                    user.achievable_rate = controller.get_adaptive_achievable_rate(user, ap)
        controller.get_resources_for_all_users()

    def rate_based_alg(self, controller):
        controller.scheduler = 'PF'
        controller.get_distances_from_users_to_aps()
        controller.get_sinr_from_users_to_aps()
        controller.get_best_sinr_and_ap_per_technology()
        for user in controller.user_list:
            controller.find_ap_with_max_achievable_rate_for_each_user(user)
        for user in controller.user_list:
            user.achievable_rate = controller.get_adaptive_achievable_rate(
                user, user.my_ap, average=False)
        controller.get_resources_for_all_users()

    def game_theory_alg(self, time_step, controller):
        controller.scheduler = 'PF'
        controller.get_distances_from_users_to_aps()
        controller.get_sinr_from_users_to_aps()
        # debug
        # self.debug_print_user_ap_snr_and_dist(controller)

        # STEP  1: random AP allocation; Allocate resources to every user; Calculate average and global payoff
        if time_step == 0:
            controller.initial_allocation()
        for user in controller.user_list:
            user.achievable_rate = controller.get_adaptive_achievable_rate(user, user.my_ap)

        controller.get_resources_for_all_users()
        assert len(controller.user_list) > 0, "Users list is empty, provide number of users"
        controller.get_average_payoffs()

        count = 1
        while True:
            print("!!!!!!!!!!!!!GT Count {}".format(count))
            controller.number_of_mutations_occurred = 0
            for user in controller.user_list:
                mutate_user_bool = controller.decide_to_mutate_user_or_not(user)
                if mutate_user_bool:
                    controller.mutate_user(user)
            controller.get_resources_for_all_users()
            controller.ensure_resouce_constraint()
            controller.get_average_payoffs()

            if count % 500 == 0:
                print("Iteration {}, now breaking".format(count))
            count += 1

            if controller.number_of_mutations_occurred == 0 or count >= 500:
                # print(" Time step {} ***Converged after {} ***".format(time_step, count))
                for user in controller.user_list:
                    if user.blocked and user.my_ap.type == 'lifi':
                        msg = "!!!Blocked user {} connect to AP {} with ach rate {}, user rate {}".format(
                            user.my_id, user.my_ap.type, user.achievable_rate, user.my_rate)
                        assert 0, msg
                    if user.my_rate < 1:
                        print("User {} achieves {} Mbps".format(user.my_id, user.my_rate))

                payoffs_dict = {}
                for user in controller.user_list:
                    payoffs_dict[user.my_id] = user.my_payoff
                break

    def run_resource_allocation_alg(self, time_step, controller):
        if self.algorithm == 'SMART' or self.algorithm == 'GT_WO_DEMAND':
            self.game_theory_alg(time_step, controller)
        elif self.algorithm == 'SNR':
            controller.with_interference = False
            self.snr_or_snir_based_alg(controller)
        elif self.algorithm == 'SINR':
            self.snr_or_snir_based_alg(controller)
        elif self.algorithm == 'RATE':
            self.rate_based_alg(controller)
        else:
            assert 0, "add other algorithms"

