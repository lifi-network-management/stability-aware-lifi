import time
import numpy as np
from simulation import Simulation
from utilities.helper_funcs import block_print
from utilities.helper_funcs import enable_print
import process_results_full
import process_res_simple
# Run game theory heuristic for the plane topology. Set the following params for the simulation:
# number of iterations of one simulation
# varying receiver orientation: either o or 1
# plot_or_not set to 1 -> plots allocation at every time step
# mean_elevation, blocking probabilities, receiver orientation

t1 = time.time()
scenario = 'plane'
debug = True
count_files = 0
# parameters
# d = {15515: [15, 5, 15], 162116: [16, 21, 16], 25155: [25, 15, 5], 1551: [15, 5, 1], 2555: [25, 5, 5],
#      25255: [25, 25, 5], 252525: [25, 25, 25], 151515: [15, 15, 15], 555: [5, 5, 5], 1555: [15, 5, 5]}
demand = 555
number_of_simualtions = 100
number_of_iterations = 50
plot_or_not = 0
save_plot = 0
block_models = [0]  # 0 (no block), 1 or 2(one prob)
block_prob_one = 0.1
block_prob_two = 0.2
num_user = 9*6  # 9*6
# algorithm = 'SMART'  # 'SMART' 'SNR', 'SINR', 'RATE' 'GT_WO_DEMAND'
algorithms = ['GT_WO_DEMAND']
ap_every_num_rows = 1
block_printing = True

mean_elevations = [0, -41 or 'random']  # 0, -41 or 'random'
for block_model in block_models:
    for mean_elevation in mean_elevations:
        if mean_elevation in [0, -41]:
            varying_rx_orientation = 1  # fixed = 0, varying=1
        else:
            varying_rx_orientation = 0
        for algorithm in algorithms:
            num_rows = int(num_user / 6)
            for vho_cost in [0, 0.4]:  # vertical handover efficiency
                if vho_cost != 0 and algorithm not in ['SMART', 'GT_WO_DEMAND']:
                    enable_print()
                    print(f"Skipping for algo {algorithm} with vho={vho_cost}")
                    continue
                enable_print()
                print(f"PLANE ****************overhead = {vho_cost}****************")
                if vho_cost == 0:
                    hho_cost = 0
                else:
                    hho_cost = 0.6
                enable_print()
                print(f"num_iter={number_of_iterations}, Rx:{varying_rx_orientation} angle={mean_elevation}, block model {block_model}, "
                      f"blockages {block_prob_one}, {block_prob_two}, alg {algorithm}, demand = {demand}")
                if block_printing:
                    block_print()
                for sim_num in range(number_of_simualtions):
                    count_files += 1
                    np.random.seed(sim_num + 5)
                    sim = Simulation(number_of_iterations, scenario, num_rows, 0,
                                                              vho_cost, varying_rx_orientation, plot_or_not,
                                                              sim_num, mean_elevation, 0, demand, hho_cost,
                                                              block_prob_one, block_prob_two, 0, 0, algorithm, save_plot,
                                                              ap_every_num_rows, block_model)
                    sim.main()


enable_print()
print("Number of sim files should be {}".format(count_files))
# process_res_simple.main('plane')
# # run_over = 'elevations' or 'block_models', params=mean_elevations or block_models
process_results_full.main('plane', run_over='elevations', params=mean_elevations)

