import numpy as np
import sys
import os
import matplotlib.colors as mcolors
import json


# Disable
def block_print():
    sys.stdout = open(os.devnull, 'w')


# Restore
def enable_print():
    sys.stdout = sys.__stdout__


def save_results_to_json(simulation, num_users, total_num_vertical, total_num_horizontal, fairness_index):
    if simulation.scenario == 'plane':
        results_name = "sim_results/plane/"
    elif simulation.scenario == 'overlap':
        results_name = "sim_results/overlap/"
    results_name += str(simulation.algorithm) + '_'
    if simulation.vho_cost == 0:
        sim_type = 'w/o'
        results_name += "{}_{}users_{}cost_".format(simulation.scenario, num_users, simulation.vho_cost)
    elif simulation.vho_cost > 0.1:
        sim_type = 'with'
        results_name += "{}_with-{}users_{}cost_".format(simulation.scenario, num_users, simulation.vho_cost)
    if simulation.mobility_flag:
        results_name += 'speed_{}_'.format(simulation.speed)
    if simulation.varying_rx_orientation:
        results_name += 'varying_rx_orient-{}_'.format(simulation.mean_elevation)
    else:
        results_name += 'fixed_rx_orient-{}_'.format(simulation.mean_elevation)
    results_name += 'block_model-{}_'.format(simulation.block_model)
    results_name += 'simnum_{}'.format(simulation.sim_num)
    results_name += '.json'
    results = {'sim_type': sim_type, 'alg': simulation.algorithm, 'sim_num': simulation.sim_num,
               'num_users': num_users, "demands": simulation.demands, 'total_num_vertical': total_num_vertical,
               'total_num_horizontal': total_num_horizontal, 'fair': fairness_index, 'number_of_iterations': simulation.number_of_iterations,
               'mean_elevation': simulation.mean_elevation, 'speed': simulation.speed,
               'scenario': simulation.scenario, 'vho': simulation.vho_cost, 'hho': simulation.hho_cost,
               'mobility': simulation.mobility_flag,
               'varying_rx_orientation': simulation.varying_rx_orientation,
               'block_model': simulation.block_model,
               'block_prob_one': simulation.block_prob_one,
               'block_prob_two': simulation.block_prob_two, "sum_tp": simulation.final_sum_tp,
               "qos": simulation.final_qos,
               "who_made_hos": simulation.final_who_made_hos,
               'num_blocked_users': simulation.count_num_blocked_users_list,
               "iteration_count": simulation.iteration_count,
               'num_hos_at_step': simulation.num_total_hos_at_step}

    with open(results_name, 'w', encoding='utf-8') as f:
        json.dump(results, f, ensure_ascii=False, indent=4)

    # assert simulation.check_hos_count == total_num_vho_handoffs, "Check handover count"


def get_num_unsatisfied_users(simulation):
    total_num_unsatisfied_users = 0
    for user in simulation.user_list:
        if user.my_rate < user.demand:
            total_num_unsatisfied_users += 1
    return total_num_unsatisfied_users


def check_that_heigt_i_ok_for_this_scenario(scenario, lifi_channel):
    # USING
    if scenario == 'plane':
        assert lifi_channel.h == 1
    elif scenario == 'overlap':
        assert lifi_channel.h == 3


def get_color_list():
    color_list = []
    for col in mcolors.BASE_COLORS:
        if col == 'white':
            continue
        color_list.append(col)
    for col in mcolors.CSS4_COLORS:
        if col in ['lime', 'maroon', 'gold', 'midnightblue', 'lightcoral', 'rosybrown', 'crimson', 'deeppink',
                   'springgreen']:
            color_list.append(col)
    for col in mcolors.TABLEAU_COLORS:
        if col == 'white':
            continue
        color_list.append(col)

    for col in mcolors.CSS4_COLORS:
        if col == 'white':
            continue
        color_list.append(col)
    return color_list

if __name__ == "__main__":
    pass
