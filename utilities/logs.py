import time, json


def log(type, event, msg=''):
    log = {'type':type, 'time': int(time.time()), 'event': event, 'message': msg}
    with open('log.json', 'a') as f:
        json.dump(log, f)
        f.write("\n")