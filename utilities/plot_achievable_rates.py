import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict
from channel_model_rf import RFChannelModel
from channel_model_lifi import LiFiChannelModel


def plot_rf_achievable_rate():
    rf_channel = RFChannelModel()
    rate_snr_dict = defaultdict(list)
    for z in range(1, 30, 1):
        snr_db = rf_channel.get_sinr_db(z)
        rate = rf_channel.get_adaptive_rate(snr_db)
        rate_snr_dict[rate / 10 ** 6].append(snr_db)
        plt.scatter(snr_db, rate, color='green')
    return rate_snr_dict


def plot_lifi_achievable_rate():
    rate_snr_dict = defaultdict(list)
    channel = LiFiChannelModel(flag_los_only=True,  scenario='SIMPLE')
    for snr_db in np.arange(1, 25, 1):
        rate = 0
        num_subcar_used = 1
        for _ in channel.subcarrier_list_lifi:
            efficiency = channel.get_spectrum_efficiency(snr_db)
            rate += 2 * channel.B_ofdm / channel.Q * efficiency
            num_subcar_used += 1
            if num_subcar_used > channel.Q / 2 - 1:
                break
        rate_snr_dict[rate/10**6].append(snr_db)
        plt.scatter(snr_db, rate/10**6, color='blue')
    return rate_snr_dict


def plot_rate_as_horizontal_line(rate_snr_dict):
    for rate, snr_list in rate_snr_dict.items():
        print(rate, snr_list)
        snr_list[-1] += 1
        if 6 in snr_list or 11 in snr_list:
            snr_list = sorted(snr_list)
            snr_list[-1] += 1
        plt.plot(snr_list, [rate] * len(snr_list), color='blue', linewidth=2)


rate_snr_dict_rf = plot_rf_achievable_rate()

rate_snr_dict_lifi = plot_lifi_achievable_rate()
plot_rate_as_horizontal_line(rate_snr_dict_lifi)

plt.tick_params(labelsize=12)
plt.ylabel("Adaptive achievable rate (Mbps)", fontsize=15)
plt.xlabel("SNR (dB)",  fontsize=15)
plt.title("RF(green) and LiFi (blue) Data Rate ")
plt.grid()
plt.savefig("adaptive_achiev_rate_lifi")
plt.show()


