from math import hypot, acos, sqrt
import matplotlib.pyplot as plt
from uniform_lattice import generate_points_with_min_distance
import numpy as np


def show_shape(patch):
    ax = plt.gca()
    ax.add_patch(patch)
    plt.axis('scaled')


def get_overlap_area(a, b, x, y, r):
    d= hypot(x - a, y - b)
    R = 2 * r
    if d < R:
        return R*r*acos(d/R) - d/2*sqrt(R*R-d*d)
    else:
        return 0


def plot_topo(coords, r, overlap_ratio, total_circles_area, total_overlap):
    for i in range(len(coords)):
        a, b = coords[i]
        circle = plt.Circle((a, b,), radius=r, fill=False)
        show_shape(circle)
        plt.scatter(a, b)
    plt.title("Overlap ratio is {} %; total overlap area {} m^2; total circle area is {} m^2".format(
        int(overlap_ratio), int(total_overlap), int(total_circles_area)))
    plt.show()


def compute_total_overlap_ratio(coords, r):
    circles_count = len(coords)
    total_circles_area = np.pi * r**2 * circles_count
    print("All circles sum area is {}".format(total_circles_area))
    total_overlap = 0
    for i in range(len(coords)):
        a, b = coords[i]
        k = i + 1
        for j in range(k,len(coords)):
            x, y = coords[j]

            if abs(a-x) > r and (b-y) > r:
                overlap_area = get_overlap_area(a, b, x, y, r)
                if overlap_area == 0:
                    continue
                # else:
                #     print("Double check")
                #     assert 0

            overlap_area = get_overlap_area(a, b, x, y, r)  # may be we have to multiply by 2 here
            total_overlap += overlap_area
            overlap_ratio_cur = int(overlap_area/(np.pi * r**2) * 100)
            if overlap_ratio_cur != 0:
                print("Current circle overlap ratio {} %".format(overlap_ratio_cur))

    overlap_ratio = round(total_overlap / total_circles_area, 3) * 100
    print("*****Overlap ratio {} %".format(overlap_ratio))
    return overlap_ratio, total_circles_area, total_overlap

def main():
    np.random.seed(1)
    r = 0.9

    # coords = generate_points_with_min_distance(n=25, shape=(10.5,10.5), min_dist=0.5)  # 6
    # coords = generate_points_with_min_distance(n=25, shape=(8.5, 8.5), min_dist=0.5)  # 14
    coords = generate_points_with_min_distance(n=25, shape=(5.5, 5.5), min_dist=0.5)  # 70
    # coords = generate_points_with_min_distance(n=25, shape=(6.5, 6.5), min_dist=0.5)  # 37
    # coords = generate_points_with_min_distance(n=25, shape=(7.5, 7.5), min_dist=0.5)  # 22

    overlap_ratio, total_circles_area, total_overlap = compute_total_overlap_ratio(coords, r)
    plot_topo(coords, r, overlap_ratio, total_circles_area, total_overlap)


if __name__ == "__main__":
    main()