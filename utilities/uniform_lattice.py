import numpy as np
import matplotlib.pyplot as plt
from math import hypot, acos, sqrt


class OverlapTopo:
    def __init__(self, r, shift, num_lifi_aps):
        self.r = r
        self.shift = shift
        self.num_lifi_aps = num_lifi_aps


    def generate_points_with_min_distance(self, n, shape, min_dist):
        # compute grid shape based on number of points
        width_ratio = shape[1] / shape[0]
        num_y = np.int32(np.sqrt(n / width_ratio)) + 1
        num_x = np.int32(n / num_y) + 1

        # create regularly spaced neurons
        x = np.linspace(-shape[1] / 2., shape[1] / 2 - 1, num_x, dtype=np.float32)  # coordinates of points
        y = np.linspace(-shape[1] / 2., shape[0] / 2 - 1, num_y, dtype=np.float32)
        coords = np.stack(np.meshgrid(x, y), -1).reshape(-1,2)
        # compute spacing
        init_dist = np.min((x[1]-x[0], y[1]-y[0]))

        # perturb points
        max_movement = (init_dist - min_dist)/2
        noise = np.random.uniform(low=-max_movement,
                                    high=max_movement,
                                    size=(len(coords), 2))
        coords += noise

        return coords

    def get_overlap_area(self, a, b, x, y, r):
        d = hypot(x - a, y - b)
        R = 2 * r
        if d < R:
            return R*r*acos(d/R) - d/2*sqrt(R*R-d*d)
        else:
            return 0

    def compute_total_overlap(self, x_coords, y_coords, r):
        total_overlap = 0
        for i in range(len(x_coords)):
            for j in range(len(x_coords)):
                if x_coords[i] == x_coords[j] and y_coords[i] == y_coords[j]:
                    continue
                overlap = self.get_overlap_area(x_coords[i], y_coords[i], x_coords[j], y_coords[j], r)
                total_overlap += overlap
        total_area = np.pi * r * r * len(y_coords)
        overlap_ratio = total_overlap/total_area * 100 / 2
        print("Number of cirles {}".format(len(x_coords)))
        print("Total overlap is {} m2".format(total_overlap))
        print("All circle area is {} m2".format(total_area))
        print("Ratio {} %".format(overlap_ratio))
        return total_overlap

    def generate_topo(self, shift):
        # radius 0.9
        # x = [-4+0.4, -2+0.2, 0, 2-0.2, 4-0.4] * 5
        # y = [-4+0.4, -4+0.4, -4+0.4, -4+0.4, -4+0.4, -2+0.2, -2+0.2, -2+0.2, -2+0.2, -2+0.2, 0, 0, 0, 0, 0,
        #      2-0.2, 2-0.2, 2-0.2, 2-0.2, 2-0.2, 4-0.4, 4-0.4, 4-0.4, 4-0.4, 4-0.4 ]

        # radius 1.8
        x_0 = 0
        y_0 = 0
        x = []
        y = []
        for i in range(self.num_lifi_aps//2):
            x.append(x_0)
            y.append(y_0)
            x.append(x_0)
            y.append(y_0-(3.6-shift))
            x_0 += (3.6-shift)
        # x = [-4 + 0.4, 0, -4 + 0.4, 0,   4 - 0.4,   4 - 0.4, -4 + 0.4, 0, 4 - 0.4]
        # y = [-4 + 0.4, -4 + 0.4, 0, 0,  -4 + 0.4,   0, 4 - 0.4, 4 - 0.4, 4 - 0.4]
        # x = x[:self.num_lifi_aps]
        # y = y[:self.num_lifi_aps]

        if shift == 0:
            return x, y

        # for i in range(len(x)):
        #     if x[i] < 0:
        #         x[i] += shift
        #     elif x[i] > 0:
        #         x[i] -= shift
        # for i in range(len(y)):
        #     if y[i] < 0:
        #         y[i] += shift
        #     elif y[i] > 0:
        #         y[i] -= shift

        return x, y


def show_shape(patch):
    ax = plt.gca()
    ax.add_patch(patch)
    plt.axis('scaled')
    # plt.show()


def main():
    # overlap_ratio_vs_shift = {1.5:0.1, 4:0.2, 7.5:0.6, 11.5:0.4, 16:0.5, 26:0.7, 32:0.8, 38:0.9, 45:1, 61:1.2, 72:1.3, 95:1.5,
    #                           108.5:1.6} # if consider overlap area twice
    overlap_ratio_vs_shift = {6: 0.4, 16: 0.8, 30: 1.2, 48: 1.5, 77: 1.9, 98: 2.1}
    shift = overlap_ratio_vs_shift[16]
    r = 1.8
    num_lifi_aps = 4
    overlap_topo = OverlapTopo(r, shift, num_lifi_aps)

    x_coords, y_coords = overlap_topo.generate_topo(shift)
    total_overlap = overlap_topo.compute_total_overlap(x_coords, y_coords, r)
    plt.figure(figsize=(40, 40))
    plt.scatter(x_coords, y_coords, s=5, marker='o')
    for i in range(len(x_coords)):
        circle = plt.Circle((x_coords[i], y_coords[i],), radius=r, fill=False)
        show_shape(circle)
    plt.show()


if __name__ == "__main__":
    main()
